import tweepy
from flask import Flask, render_template, jsonify, request

__author__ = 'Salvatore Fiorenza'
__copyright__ = 'Copyright (c) 2016 Salvatore Fiorenza'
__license__ = 'MIT'
__version__ = '0.1'
__maintainer__ = 'Salvatore Fiorenza'
__email__ = 'salvatore.fiorenza.8@gmail.com'
__status__ = 'Development'

app = Flask(__name__)


@app.route('/search-tweets')
def search_tweets():
    query = request.args.get('query', None, type=str)
    consumer_key = ''
    consumer_secret = ''
    access_token = ''
    access_token_secret = ''
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    search = api.search(q=query)
    response = jsonify(render_template('feed.html', search=search, query=query))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


if __name__ == "__main__":
    app.run()
